/* 订单详情*/
function openOrderDetail(orderId){
	api.openWin({
	    name: 'orderDetail',
	    url: 'win_oderDetail.html',
	    pageParam:{orderId:orderId},
	    bounces:false,
    });
}
//提示框
function alertMes(content){	
	var dialogBox = api.require('dialogBox');
	dialogBox.alert ({
	    texts: {
	        title: '', 
	        content:content,           
	        leftBtnTitle: '我知道了' 
	    },
	    styles:{
	        bg: '#fff',
	        corner: 8, 
	        w: 300,
	        title:{   
	            marginT : 20,
				icon : 'widget://res/face1.png',
				iconSize:40,
	            titleSize : 14,
	            titleColor : '#ff7f00'
	        },
	        content:{  
	            color: '#ff0000',         
	            size: 14          
	        },
	        left:{            
	            marginB: 7,      
	            marginL: 80,     
	            w: 140,           
	            h: 35,
	            corner: 2,       
	            bg: '#ff7f00', 
	            color:'#fff',  
	            size: 12    
	        },
	    }
	},function(ret){
	    if (ret.eventType == 'left') {
	        var dialogBox = api.require('dialogBox');
	        dialogBox.close ({
	            dialogName: 'alert'
	        });
	    }
	});	
}