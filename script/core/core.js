var hostUrl = 'http://192.168.203.2:9095';
//var hostUrl = 'http://192.168.43.207:9095';
var hostUrlMes = '192.168.203.2:81/api/Service';
function ajaxLocalMes(data,  methodName, callback) {
	var jsondata = JSON.stringify(data);
	api.ajax({
		url : hostUrlMes + '/' + methodName,
		method : 'post',
		// cache : true,
		data : {
			values : data
		}
	}, function(ret, err) {
		callback.call(this, ret, err);
	});
}
function ajaxLocal(data, controllerName, methodName, callback) {
	var jsondata = JSON.stringify(data);
	api.ajax({
		url : hostUrl + '/' + controllerName + '/' + methodName,
		method : 'post',
		cache : true,
		data : {
			values : data
		}
	}, function(ret, err) {
		callback.call(this, ret, err);
	});
}
/***********************************Entity***********************************/
var UserEntity = function() {
	this.objId
	this.cellPhoneNumber//手机号码
	this.userName//用户名
	this.password//密码
	this.deleteState = "0";
	//删除状态
	this.userPoint = 0.0;
	//消费者积分
	this.userPicture//
	this.nickName//昵称
	this.sex//性别 0=女1=男
	this.userMail//邮箱
	this.createTime//消费者创建（注册）时间
	this.num//用户序号

}
/**
 * 构造用户JSON对象
 * id：用户id
 * userName：用户登录名称
 * password：用户密码
 * isRemember：是否记住密码
 * loginDate：用户登录时间
 * isLogin：是否已登录
 */
function jsonUserInit(id, userName, password, isRemember, loginDate, isLogin) {
	return {
		id : id,
		userName : userName,
		password : password,
		isRemember : isRemember,
		loginDate : loginDate,
		isLogin : isLogin
	};
}

/**
 * 计算两个时间的时间差
 * beginTime：开始时间Date类型
 * endTime：结束时间Date类型
 * 返回：相差的天数
 */
function timeDifference(beginTime, endTime) {
	var ms = endTime.getTime() - beginTime.getTime();
	return Math.floor(ms / (24 * 3600 * 1000))
}

/**
 * 验证本地用户是否已登录
 * 返回：true用户已登录，否则返回false
 */
function isLogin() {
	//读取本地用户信息
	var user = $api.getStorage("storageUser");
	if (user) {
		//已获取本地用户数据，验证用户登录状态
		if (user.isLogin) {
			//登录已登录，计算登录时间是否超时
			var loginDate = new Date(user.loginDate);
			var curDate = new Date();
			if (timeDifference(loginDate, curDate) <= 7) {
				//当前用户登录成功
				return true;
			} else {
				//当前用户登录超时
				return false;
			}
		} else {
			//当前用户未登录
			return false;
		}
	} else {
		//未获取到本地用户信息
		return false;
	}
}

//打开菜品详情页
function dishesDetail(dishesId, price) {
	api.openWin({
		name : "dishDetail",
		url : '../../html/ShopHome/dishDetail_window.html',
		rect : {
			x : 0,
			y : 0,
			w : 'auto',
			h : 'auto'
		},
		bounces : false,
		delay : 200,
		pageParam : {
			dishId : dishesId,
			price : price
		}
	});
}

//打开套餐详情页面
function groupDetail(groupId, price) {
	api.openWin({
		name : "dishDetail",
		url : '../ShopHome/dishDetail_window.html',
		rect : {
			x : 0,
			y : 0,
			w : 'auto',
			h : 'auto'
		},
		bounces : false,
		delay : 200,
		pageParam : {
			groupId : groupId,
			price : price,
			isDishGroup : true//是否是套餐，可选
		}
	});
}

//获取当前系统时间
function getNowFormatDate() {
	var now = new Date();
	var year = now.getFullYear();
	var month = (now.getMonth() + 1).toString();
	var day = (now.getDate()).toString();
	var hh = now.getHours().toString();
	//时
	var mm = now.getMinutes().toString();
	//分
	var ss = now.getSeconds().toString();
	if (month.length == 1) {
		month = "0" + month;
	}
	if (day.length == 1) {
		day = "0" + day;
	}
	if (hh.length == 1) {
		hh = "0" + hh;
	}
	if (mm.length == 1) {
		mm = "0" + mm;
	}
	if (ss.length == 1) {
		ss = "0" + ss;
	}
	var dateTime = year + "-" + month + "-" + day + " " + hh + ":" + mm + ":" + ss + ".000";
	return dateTime;
}
function checkProcess(gx,zndy){
var processID;
	    if(zndy=='DJCX'){
			     	line='大件线';
			     	znUint='成型';
			     	if(gx=="射芯"){
			     		processID='01.02.01';
					}else if(gx=="预组芯"){
					    processID='01.02.02';
					}else if(gx=="浸涂"){
						processID='01.02.03';
					}else if(gx=="组芯"){
					     processID='01.03.01';
					}else if(gx=="配箱"){
						processID='01.03.02';
					}

			     }else if(zndy=='XJCX'){
			        if(gx=="3D打印"){
			     	 	processID='02.02.01';
			    	//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order2, StaticString.s_D3daying, "Running", startTime, endTime);
					}else if(gx=="施涂"){
						processID='02.02.04';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order2, StaticString.s_shitu, "Running", startTime, endTime);
					}else if(gx=="组芯"){
						processID='02.03.01';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_zuxin, "Running", startTime, endTime);
					}else if(gx=="配箱"){
						processID='02.03.02';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}else if(gx=="清砂"){
						processID='02.02.03';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}else if(gx=="仓储"){
						processID='02.02.06';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}

			     }else if(zndy=='DJJZ'){
			        line='大件线';
			     	znUint='精整';
			     	 if(gx=="打箱"){
			     	 	processID='01.07.01';
			    	//plan=pu.get_plan(StaticString.BigLine, StaticString.Order7, StaticString.fanxiang, "Running", startTime, endTime);
					}else if(gx=="去冒口"){
						processID='01.08.01';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.qujiaomaokou, "Running", startTime, endTime);
					}else if(gx=="热处理"){
						processID='01.08.02';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.rechuli, "Running", startTime, endTime);
					}else if(gx=="抛丸"){
						processID='01.08.03';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.paowan, "Running", startTime, endTime);
					}else if(gx=="精修"){
						processID='01.08.04';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.jingxiu, "Running", startTime, endTime);
					}else if(gx=="检测"){
						processID='01.08.05';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.jianche, "Running", startTime, endTime);
					}else if(gx=="喷漆"){
						processID='01.08.06';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.penqi, "Running", startTime, endTime);
					}

			     }else if(zndy=='XJJZ'){
			        line='小件线';
			     	znUint='精整';
			     	if(gx=="打箱"){
			     		processID='02.07.01';
			    	//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order7, StaticString.s_fanxiang, "Running", startTime, endTime);
					}else if(gx=="去冒口"){
						processID='02.08.01';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_qujiaomaokou, "Running", startTime, endTime);
					}else if(gx=="热处理"){
						processID='02.08.02';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_rechuli, "Running", startTime, endTime);
					}else if(gx=="抛丸"){
						processID='02.08.03';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_paowan, "Running", startTime, endTime);
					}else if(gx=="精修"){
						processID='02.08.04';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_jingxiu, "Running", startTime, endTime);
					}else if(gx=="检测"){
						processID='02.08.05';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_jianche, "Running", startTime, endTime);
					}else if(gx=="喷漆"){
						processID='02.08.06';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_penqi, "Running", startTime, endTime);
					}
			     }
	return processID;
}
function checkUnit(gx,zndy){
var processID;
var line;
var znUint;
	    if(zndy=='DJCX'){
			     	line='大件线';
			     	znUint='成型';
			     	if(gx=="射芯"){
			     		processID='01.02.01';
					}else if(gx=="预组芯"){
					    processID='01.02.02';
					}else if(gx=="浸涂"){
						processID='01.02.03';
					}else if(gx=="组芯"){
					     processID='01.03.01';
					}else if(gx=="配箱"){
						processID='01.03.02';
					}

			     }else if(zndy=='XJCX'){
			     line='小件线';
			     	znUint='成型';
			        if(gx=="3D打印"){
			     	 	processID='02.02.01';
			    	//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order2, StaticString.s_D3daying, "Running", startTime, endTime);
					}else if(gx=="施涂"){
						processID='02.02.04';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order2, StaticString.s_shitu, "Running", startTime, endTime);
					}else if(gx=="组芯"){
						processID='02.03.01';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_zuxin, "Running", startTime, endTime);
					}else if(gx=="配箱"){
						processID='02.03.02';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}
					else if(gx=="清砂"){
						processID='02.02.03';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}else if(gx=="仓储"){
						processID='02.02.06';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}

			     }else if(zndy=='DJJZ'){
			        line='大件线';
			     	znUint='精整';
			     	 if(gx=="打箱"){
			     	 	processID='01.07.01';
			    	//plan=pu.get_plan(StaticString.BigLine, StaticString.Order7, StaticString.fanxiang, "Running", startTime, endTime);
					}else if(gx=="去冒口"){
						processID='01.08.01';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.qujiaomaokou, "Running", startTime, endTime);
					}else if(gx=="热处理"){
						processID='01.08.02';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.rechuli, "Running", startTime, endTime);
					}else if(gx=="抛丸"){
						processID='01.08.03';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.paowan, "Running", startTime, endTime);
					}else if(gx=="精修"){
						processID='01.08.04';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.jingxiu, "Running", startTime, endTime);
					}else if(gx=="检测"){
						processID='01.08.05';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.jianche, "Running", startTime, endTime);
					}else if(gx=="喷漆"){
						processID='01.08.06';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.penqi, "Running", startTime, endTime);
					}

			     }else if(zndy=='XJJZ'){
			        line='小件线';
			     	znUint='精整';
			     	if(gx=="打箱"){
			     		processID='02.07.01';
			    	//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order7, StaticString.s_fanxiang, "Running", startTime, endTime);
					}else if(gx=="去冒口"){
						processID='02.08.01';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_qujiaomaokou, "Running", startTime, endTime);
					}else if(gx=="热处理"){
						processID='02.08.02';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_rechuli, "Running", startTime, endTime);
					}else if(gx=="抛丸"){
						processID='02.08.03';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_paowan, "Running", startTime, endTime);
					}else if(gx=="精修"){
						processID='02.08.04';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_jingxiu, "Running", startTime, endTime);
					}else if(gx=="检测"){
						processID='02.08.05';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_jianche, "Running", startTime, endTime);
					}else if(gx=="喷漆"){
						processID='02.08.06';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_penqi, "Running", startTime, endTime);
					}
			     }
	return znUint;
}
function checkLine(gx,zndy){
//alert("122"+gx+zndy);
var processID;
var line;
var znUint;
	    if(zndy=='DJCX'){
			     	line='大件线';
			     	znUint='成型';
			     	if(gx=="射芯"){
			     		processID='01.02.01';
					}else if(gx=="预组芯"){
					    processID='01.02.02';
					}else if(gx=="浸涂"){
						processID='01.02.03';
					}else if(gx=="组芯"){
					     processID='01.03.01';
					}else if(gx=="配箱"){
						processID='01.03.02';
					}

			     }else if(zndy=='XJCX'){
			     line='小件线';
			     	znUint='成型';
			        if(gx=="3D打印"){
			     	 	processID='02.02.01';
			    	//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order2, StaticString.s_D3daying, "Running", startTime, endTime);
					}else if(gx=="施涂"){
						processID='02.02.04';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order2, StaticString.s_shitu, "Running", startTime, endTime);
					}else if(gx=="组芯"){
						processID='02.03.01';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_zuxin, "Running", startTime, endTime);
					}else if(gx=="配箱"){
						processID='02.03.02';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}
					else if(gx=="清砂"){
						processID='02.02.03';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}else if(gx=="仓储"){
						processID='02.02.06';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order3, StaticString.s_peixiang, "Running", startTime, endTime);
					}

			     }else if(zndy=='DJJZ'){
			        line='大件线';
			     	znUint='精整';
			     	 if(gx=="打箱"){
			     	 	processID='01.07.01';
			    	//plan=pu.get_plan(StaticString.BigLine, StaticString.Order7, StaticString.fanxiang, "Running", startTime, endTime);
					}else if(gx=="去冒口"){
						processID='01.08.01';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.qujiaomaokou, "Running", startTime, endTime);
					}else if(gx=="热处理"){
						processID='01.08.02';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.rechuli, "Running", startTime, endTime);
					}else if(gx=="抛丸"){
						processID='01.08.03';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.paowan, "Running", startTime, endTime);
					}else if(gx=="精修"){
						processID='01.08.04';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.jingxiu, "Running", startTime, endTime);
					}else if(gx=="检测"){
						processID='01.08.05';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.jianche, "Running", startTime, endTime);
					}else if(gx=="喷漆"){
						processID='01.08.06';
						//plan=pu.get_plan(StaticString.BigLine, StaticString.Order8, StaticString.penqi, "Running", startTime, endTime);
					}

			     }else if(zndy=='XJJZ'){
			        line='小件线';
			     	znUint='精整';
			     	if(gx=="打箱"){
			     		processID='02.07.01';
			    	//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order7, StaticString.s_fanxiang, "Running", startTime, endTime);
					}else if(gx=="去冒口"){
						processID='02.08.01';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_qujiaomaokou, "Running", startTime, endTime);
					}else if(gx=="热处理"){
						processID='02.08.02';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_rechuli, "Running", startTime, endTime);
					}else if(gx=="抛丸"){
						processID='02.08.03';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_paowan, "Running", startTime, endTime);
					}else if(gx=="精修"){
						processID='02.08.04';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_jingxiu, "Running", startTime, endTime);
					}else if(gx=="检测"){
						processID='02.08.05';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_jianche, "Running", startTime, endTime);
					}else if(gx=="喷漆"){
						processID='02.08.06';
						//plan=pu.get_plan(StaticString.SmallLine, StaticString.Order8, StaticString.s_penqi, "Running", startTime, endTime);
					}
			     }
	return line;
}
